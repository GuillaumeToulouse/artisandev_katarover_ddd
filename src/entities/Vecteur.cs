namespace src.entities
{
        public struct Vecteur
    {
        public Vecteur(int x,
                       int y)
        {
            X = x;
            Y = y;
        }
        public Vecteur Reverse()
        {
            return new Vecteur(
            -(this.X),
            -(this.Y));
        }
        public readonly int X { get; }
        public readonly int Y { get; }
    }
}
