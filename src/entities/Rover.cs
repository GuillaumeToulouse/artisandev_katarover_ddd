using System;

namespace src.entities
{
    public partial class Rover
    {
        Position position;
        Directions direction;
        static System.Collections.Generic.Dictionary<Directions, Vecteur> translationsRules;

        static Rover()
        {
            translationsRules = new System.Collections.Generic.Dictionary<Directions, Vecteur>();
            translationsRules.Add(Directions.North, new Vecteur(0, 1));
            translationsRules.Add(Directions.East, new Vecteur(1, 0));
            translationsRules.Add(Directions.South, new Vecteur(0, -1));
            translationsRules.Add(Directions.West, new Vecteur(-1, 0));
        }

        public Rover(int x, int y, Directions direction)
        {
            this.position = new Position(x, y);
            this.direction = direction;

        }

        public Rover(Position position, Directions direction)
        {
            this.position = position;
            this.direction = direction;
        }


        public Position Position
        {
            get { return this.position; }
        }

        public Directions Direction
        {
            get { return this.direction; }
        }

        public void MoveForward()
        {
            var vecteur = translationsRules[this.direction];
            this.position = this.position.Translation(vecteur);
        }

        public void MoveBackward()
        {
            var vecteur = translationsRules[this.direction];
            this.position = this.position.Translation(vecteur.Reverse());
        }

        
    }
}