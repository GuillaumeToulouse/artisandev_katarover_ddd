using src.entities;
using System;
using System.Collections.Generic;

namespace src.entities
{
    public interface Orientation
    {
        Orientation AtLeft();
        Orientation AtRight();
    }

    public static class Orientations
    {
        static System.Collections.Generic.Dictionary<Directions, Func<Orientation>> directionsConstructor;
        static Orientations()
        {
            directionsConstructor = new Dictionary<Directions, Func<Orientation>>();
            directionsConstructor.Add(Directions.North, () => new North());
            directionsConstructor.Add(Directions.East, () => new East());
            directionsConstructor.Add(Directions.South, () => new South());
            directionsConstructor.Add(Directions.West, () => new West());
        }
        public static Orientation Build(Directions d)
        {
            return directionsConstructor[d]();
        }

        private struct North : Orientation
        {
            public Orientation AtLeft()
            {
                return new East();
            }
            public Orientation AtRight()
            {
                return new West();
            }
        }
        private struct East : Orientation
        {
            public Orientation AtLeft()
            {
                return new South();
            }
            public Orientation AtRight()
            {
                return new North();
            }
        }
        private struct South : Orientation
        {
            public Orientation AtLeft()
            {
                return new West();
            }
            public Orientation AtRight()
            {
                return new East();
            }
        }
        private struct West : Orientation
        {
            public Orientation AtLeft()
            {
                return new North();
            }
            public Orientation AtRight()
            {
                return new South();
            }
        }
    }
}