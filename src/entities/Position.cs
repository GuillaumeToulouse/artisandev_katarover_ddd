using System;

namespace src.entities
{    
    public struct Position
    {
        readonly int x;
        readonly int y;

        public Position(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public int X { get => x; }
        public int Y { get => y; }

        public override string ToString()
        {
            return $"{this.x}:{this.y}";
        }

        public Position Translation(Vecteur vecteur)
        {
            return Translation(vecteur.X, vecteur.Y);
        }

        public Position Translation(int deltaX, int deltaY)
        {
            return new Position( this.X + deltaX, this.Y + deltaY  );
        }



    }
}