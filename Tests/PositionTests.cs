using NUnit.Framework;
using src.entities;

namespace Tests
{
    public class POsitionTests
    {

        [Test]
        public void Equality()
        {
            var pos1 = new Position(0, 1);
            var pos2 = new Position(1, 1);
            var pos3 = new Position(1, 1);

            Assert.That(pos1, Is.Not.EqualTo(pos2));
            Assert.That(pos2, Is.EqualTo(pos3));
        }

           [Test]
           public void Translate()
           {
               var pos1 = new Position(0, 1);
               var res = pos1.Translation(3,2);
               Assert.That(res, Is.EqualTo(new Position(3,3)));
           }
    }
}