using NUnit.Framework;
using src.entities;

namespace Tests
{
    public class RoverTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void InitialPositionOfRoverShouldBeGivenWhenTheRoverIsCreated()
        {
            //ARRANGE                         
            var sut =  new RoverDataBuilder()
            .WithPosition(new Position(1, 2))
            .WithDirection( Directions.North)
            .Build();
            // avec cette version, il m'est impossible d'obtenir un objet qui ne serait pas bien initialisé
            // grace au typage statique
            // je n'ai plus besoin de faire des tests, avec des exceptions qui pourraient être levées
            // si ca compile, c'est que c'est OK !!!!!!

            //ASSERT
            Assert.That(sut.Position, Is.EqualTo(new Position(1, 2)));
            Assert.That(sut.Direction, Is.EqualTo(Directions.North));
        }        

        [Test]
        public void RoverShouldMoveForward()
        {   
              var sut =  new RoverDataBuilder()
             .WithPosition(new Position(1,2))
             .WithDirection(Directions.North)
             .Build();
         
            sut.MoveForward();
         
            Assert.That(sut.Position, Is.EqualTo(new Position(1, 3)));
        }

        [Test]
        public void RoverShouldMoveForwardWithAnotherPosition()
        {   
              var sut =  new RoverDataBuilder()
             .WithPosition(new Position(1,0))
             .WithDirection(Directions.North)
             .Build();
         
            sut.MoveForward();
         
            Assert.That(sut.Position, Is.EqualTo(new Position(1, 1)));
        }

        [Test]
        public void RoverShouldMoveForwardWithDirectionEast()
        {   
              var sut =  new RoverDataBuilder()
             .WithPosition(new Position(1,0))
             .WithDirection(Directions.East)
             .Build();
         
            sut.MoveForward();
         
            Assert.That(sut.Position, Is.EqualTo(new Position(2, 0)));
        }

         [Test]
        public void RoverShouldMoveForwardWithDirectionSouth()
        {   
              var sut =  new RoverDataBuilder()
             .WithPosition(new Position(1,1))
             .WithDirection(Directions.South)
             .Build();
         
            sut.MoveForward();
         
            Assert.That(sut.Position, Is.EqualTo(new Position(1, 0)));
        }

         [Test]
        public void RoverShouldMoveForwardWithDirectionWest()
        {   
              var sut =  new RoverDataBuilder()
             .WithPosition(new Position(10,10))
             .WithDirection(Directions.West)
             .Build();
         
            sut.MoveForward();         
            Assert.That(sut.Position, Is.EqualTo(new Position(9, 10)));

            //sut.MoveBackward(); 
            //Assert.That(sut.Position, Is.EqualTo(new Position(10, 10)));
        }

           [Test]
        public void RoverShouldMoveBackwardWithDirectionWest()
        {   
              var sut =  new RoverDataBuilder()
             .WithPosition(new Position(10,10))
             .WithDirection(Directions.West)
             .Build();
         
            sut.MoveBackward();
         
            Assert.That(sut.Position, Is.EqualTo(new Position(11, 10)));
        }



    }
}
