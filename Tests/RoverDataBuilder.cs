using System;
using src.entities;

namespace Tests
{
    internal class RoverDataBuilder
    {
        internal RoverDataBuilderWithPosition WithPosition(Position position)
        {
            return new RoverDataBuilderWithPosition(position);
        }

        internal class RoverDataBuilderWithPosition
        {
            private readonly Position position;
            public RoverDataBuilderWithPosition(Position position)
            {
                this.position = position;
            }
            internal RoverDataBuilderCompleted WithDirection(Directions north)
            {
                return new RoverDataBuilderCompleted(this.position, north);
            }
        }

        internal class RoverDataBuilderCompleted
        {
            private readonly Directions Direction;

            private readonly Position Position;

            public RoverDataBuilderCompleted(Position position, Directions north)
            {
                this.Direction = north;
                this.Position = position;
            }

            internal Rover Build()
            {
                return new Rover(this.Position, this.Direction);
            }
        }
    }
}