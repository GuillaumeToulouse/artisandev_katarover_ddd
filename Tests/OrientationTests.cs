using NUnit.Framework;
using src.entities;

namespace Tests
{
    public class OrientationTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void FromNorthTurnLeft()
        {   
            var north = Orientations.Build(Directions.North);
            var res = north.AtLeft();
            Assert.That(res, Is.EqualTo( Orientations.Build(Directions.East)));
            res = res.AtLeft();
            Assert.That(res, Is.EqualTo( Orientations.Build(Directions.South)));
             res = res.AtLeft();
            Assert.That(res, Is.EqualTo( Orientations.Build(Directions.West)));
              res = res.AtLeft();
            Assert.That(res, Is.EqualTo( Orientations.Build(Directions.North)));
        }

         [Test]
        public void FromNorthTurnRight()
        {   
            var north = Orientations.Build(Directions.North);
            var res = north.AtRight();
            Assert.That(res, Is.EqualTo( Orientations.Build(Directions.West)));
             res = res.AtRight();
            Assert.That(res, Is.EqualTo( Orientations.Build(Directions.South)));
              res = res.AtRight();
            Assert.That(res, Is.EqualTo( Orientations.Build(Directions.East)));
              res = res.AtRight();
            Assert.That(res, Is.EqualTo( Orientations.Build(Directions.North)));
            
        }
    }
}